#define TODEG 57.29577951          //Conversion factors for degrees & radians
#define TORAD 0.017453293         //The AVR cannot reach this level of precision but w/e

#include "ros/ros.h"
#include "quadrifoglio_msgs/baseDriveCmd.h"

#include <cmath>
#include <termios.h>
#include <unistd.h>
#include <fcntl.h>

#include <thread>		//Multithreading
#include <mutex>

std::mutex teleopMsgMutex;  //Mutex for accessing teleop msg


int getch()
{
  static struct termios oldt, newt;
  tcgetattr( STDIN_FILENO, &oldt);           // save old settings
  newt = oldt;
  newt.c_lflag &= ~(ICANON);                 // disable buffering      
  tcsetattr( STDIN_FILENO, TCSANOW, &newt);  // apply new settings

  int c = getchar();  // read character (non-blocking)

  tcsetattr( STDIN_FILENO, TCSANOW, &oldt);  // restore old settings
  return c;
}

void AckermanDrive(float throttle, float frontAngle, float rearAngle, quadrifoglio_msgs::baseDriveCmd* msg) { //This calculates wheel ackerman angles according to front and rear bicycle model angles
  const float wheelbase = 25.3;  //Robot wheelbase and track, for ackerman angle calculations
  const float track = 15.5;

  float b1 = wheelbase / 2; //Constant terms of lines
  float b2 = -wheelbase / 2;
  float k1 = tan(TORAD * (frontAngle)); //Kulmakerroin of line
  float k2 = tan(TORAD * (rearAngle));

  float x3 = (b2 - b1) / (k1 - k2);   //Solving convergence points, x
  float y3 = k1 * x3 + b1;            //y

  float upLeftAngle;
  float upRightAngle;
  float downLeftAngle;
  float downRightAngle;

  if(abs(frontAngle-rearAngle)< 0.1){ //Angles are close together, lines wont converge
       upLeftAngle = frontAngle;
       upRightAngle = frontAngle;
       downLeftAngle = rearAngle;
       downRightAngle = rearAngle;
  }
  else{
    float tempAngle = TODEG * (-atan((-(track / 2) - x3) / ((wheelbase / 2) - y3)));
    upLeftAngle = (tempAngle > 0) ?  90.0 - tempAngle : -90.0 + (tempAngle * -1); //Wheel position - convergence point, right angled triangle

    tempAngle = TODEG * (-atan(((track / 2) - x3) / ((wheelbase / 2) - y3)));
    upRightAngle = (tempAngle > 0) ?  90.0 - tempAngle : -90.0 + (tempAngle * -1);

    tempAngle = TODEG * (-atan((-(track / 2) - x3) / (-(wheelbase / 2) - y3)));
    downLeftAngle = (tempAngle > 0) ?  90.0 - tempAngle : -90.0 + (tempAngle * -1);

    tempAngle = TODEG * (-atan(((track / 2) - x3) / (-(wheelbase / 2) - y3)));
    downRightAngle = (tempAngle > 0) ?  90.0 - tempAngle : -90.0 + (tempAngle * -1);

  }

  if (abs(frontAngle) > 90 && abs(rearAngle) > 90 && ((frontAngle > 0) ^ (rearAngle > 0))) {
    msg->frontLeftServo=4300; msg->frontRightServo=1700;
    msg->rearLeftServo=1700; msg->rearRightServo=4300;
    if (frontAngle > 0) {
      msg->frontLeftMot = throttle+255; msg->frontRightMot =-throttle+255; msg->rearLeftMot = throttle+255; msg->rearRightMot = -throttle + 255;
    }
    else {
      msg->frontLeftMot = -throttle+255; msg->frontRightMot = throttle+255; msg->rearLeftMot = -throttle + 255;msg->rearRightMot = throttle +255;
    }
  }
  else {
    msg->frontLeftServo = 22.2222 * upLeftAngle + 3000; msg->frontRightServo = 22.2222 * upRightAngle + 3000;
    msg->rearLeftServo = 22.2222 * downLeftAngle + 3000; msg->rearRightServo = 22.2222 * downRightAngle + 3000;
    msg->frontLeftMot = throttle+255; msg->frontRightMot =throttle+255; msg->rearLeftMot = throttle+255; msg->rearRightMot = throttle+255;
  }
  
}

void DoAckerman(quadrifoglio_msgs::baseDriveCmd* msg){
  float throttle = 0;
  float front = 0;
  float rear = 0;
  while(1){
    char c = (char)getch();
        ROS_INFO("Character: %c",c);
        switch(c){
          case 'a':
            front = 45;
            rear = -45;
            break;
          case 'd':
            front = -45;
            rear = 45;
            break;
          case 's':
            front = 0;
            rear = 0;
            break;
          case 'q':
            front = 10;
            rear = -10;
            break;
          case 'e':
            front = -10;
            rear = 10;
            break;
          case 'z':
            front = 95;
            rear = -95;
            break;
          case 'c':
            front = -95;
            rear = 95;
            break;
          case 'w':
            throttle += 51;
            if(throttle> 255) throttle = 255;
            break;
          case 'x':
            throttle -=51;
            if(throttle < -255) throttle = -255;
            break;
          case ' ':
            front = 0;
            rear = 0;
            throttle = 0;
            break;
          default:
            break;

        }
        teleopMsgMutex.lock();
        AckermanDrive(throttle, front, rear, msg);
        teleopMsgMutex.unlock();
  }
}

int main(int argc, char **argv){
    ros::init(argc, argv, "quadrifoglio_keyboard_teleop_node");
    ros::NodeHandle n;
    ros::Rate loop_rate(30);
 

    ros::Publisher baseDrivePub = n.advertise<quadrifoglio_msgs::baseDriveCmd>("quadrifoglio/baseDriveCmd", 1);
    quadrifoglio_msgs::baseDriveCmd driveCmd;
    driveCmd.frontLeftServo = 3000;
    driveCmd.frontRightServo = 3000;
    driveCmd.rearLeftServo = 3000;
    driveCmd.rearRightServo = 3000;
    driveCmd.frontLeftMot = 255;
    driveCmd.frontRightMot = 255;
    driveCmd.rearLeftMot = 255;
    driveCmd.rearRightMot = 255;

    //fcntl (0, F_SETFL, O_NONBLOCK);
    std::thread t1(DoAckerman, &driveCmd);

    while(ros::ok()){
        teleopMsgMutex.lock();
        baseDrivePub.publish(driveCmd);
        teleopMsgMutex.unlock();
        ros::spinOnce();
        loop_rate.sleep();
    }
    return 0;
}